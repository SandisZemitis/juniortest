<?php
class Product{
    public $sku;
    public $name;
    public $price;
    public $table;
    function __construct($sku,$name,$price,$table){
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->table = $table;
    }
}
class Furniture extends Product{

    public $height;
    public $width;
    public $length;

    public function __construct($sku,$name,$price,$table,$height,$width,$length){
        parent::__construct($sku,$name,$price,$table);
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }

    public function insert(){
      $sql = "INSERT INTO $this->table (sku,name, price, weight, size, height, width, length)
  VALUES ('$this->sku', '$this->$name','$this->price','$this->weight','$this->size','$this->height','$this->width','$this->length')";
    }
}
class Book extends Product{
    public $weight;
    public function __construct($sku,$name,$price,$table,$weight){
        parent::__construct($sku,$name,$price,$table);
        $this->weight = $weight;

    }

}

class Cd extends Product{
    public $size;
    public function __construct($sku,$name,$price,$table,$size){
        parent::__construct($sku,$name,$price,$table);
        $this->size = $size;
    }
}
