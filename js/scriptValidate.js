$(document).ready(function() {
    $('#save').click(function() {
        var sku = $('#sku').val();
        var name = $('#name').val();
        var price = $('#price').val();
        var type = $('#type option:selected').text();
        var sku_regex = /^[A-Za-z0-9]+$/;
        var name_regex = /^[a-zA-Z\s]+$/;
        var price_regex = /^[0-9]{1,3}(.[0-9]{2})$/;
        var whl_regex = /^[0-9]{0,10}(.[0-9]{2})?$/;
        var whlInc_regex = /^[0-9]{1,3}(.[0-9]{2})?$/;
        var size_regex = /^[0-9]{3,6}(.[0-9]{2})?$/;
        var book_regex = /^[0-9]{0,2}(.[0-9]{2})?$/;
        var cd_regex = /^\d+$/;
        var skuCheck = false;
        var nameCheck = false;
        var priceCheck = false;
        var switchCheck = false;
        var typeCheck = false;
        var type2Check = true;
        var type3Check = true;

        if (sku.length < 1) {
            $('#skuError').html("This field is required!");
            skuCheckheck = false;
        } else if (sku.length > 10) {
            $('#skuError').html("Maximum 10 characters!");
            skuCheck = false;
        } else if (!sku.match(sku_regex)) {
            $('#skuError').html("Numbers and letters only!");
            skuCheck = false;
        } else {
            $('#skuError').html('');
            skuCheck = true;
        }

        if (name.length < 1) {
            $('#nameError').html('This field is required!');
            nameCheck = false;
        } else if (!name.match(name_regex)) {
            $('#nameError').html('Letters only!');
            nameCheck = false;
        } else if (name.length > 50) {
            $('#nameError').html('Maximum 50 characters!');
            nameCheck = false;
        } else {
            $('#nameError').html('');
            nameCheck = true;
        }

        if (price.length < 1) {
            $('#priceError').html('This field is required!');
            priceCheck = false;
        } else if (!price.match(price_regex)) {
            $('#priceError').html('Format xxx.xx!');
            priceCheck = false;
        } else {
            $('#priceError').html('');
            priceCheck = true;
        }

        if (type == 'Type switcher') {
            $('#switchError').html('You must choose a type!');
            switchCheck = false;
        } else {
            $('#switchError').html('');
            switchCheck = true;
        }

        if (type == 'Book') {
            var weight = $('#weight').val();
            if (weight.length < 1) {
                $('#typeError').html('This field is required!');
                typeCheck = false;
            } else if (!weight.match(whl_regex)) {
                $('#typeError').html('Numbers only! If using decimal then 2 numbers after dot.');
                typeCheck = false;
            } else if (!weight.match(book_regex)) {
                $('#typeError').html('Book weight incorrect!');
                typeCheck = false;
            } else {
                $('#typeError').html('');
                typeCheck = true;
            }
        }

        if (type == 'CD') {
            var size = $('#size').val();
            if (size.length < 1) {
                $('#typeError').html('This field is required!');
                typeCheck = false;
            } else if (!size.match(cd_regex)) {
                $('#typeError').html('Numbers only!');
                typeCheck = false;
            } else if (!size.match(size_regex)) {
                $('#typeError').html('CD size incorrect');
                typeCheck = false;
            } else {
                $('#typeError').html('');
                typeCheck = true;
            }
        }

        if (type == 'Furniture') {
            var height = $('#height').val();
            var width = $('#width').val();
            var length = $('#length').val();

            if (height.length < 1) {
                $('#typeError').html('This field is required!');
                typeCheck = false;
            } else if (!height.match(whl_regex)) {
                $('#typeError').html('Numbers only! If using decimal then 2 numbers after dot.');
                typeCheck = false;
            } else if (!height.match(whlInc_regex)) {
                $('#typeError').html('Height incorrect!');
                typeCheck = false;
            } else {
                $('#typeError').html('');
                typeCheck = true;
            }

            if (width.length < 1) {
                $('#type2Error').html('This field is required!');
                type2Check = false;
            } else if (!width.match(whl_regex)) {
                $('#type2Error').html('Numbers only! If using decimal then 2 numbers after dot.');
                type2Check = false;
            } else if (!width.match(whlInc_regex)) {
                $('#type2Error').html('Width incorrect!');
                type2Check = false;
            } else {
                $('#type2Error').html('');
                type2Check = true;
            }

            if (length.length < 1) {
                $('#type3Error').html('This field is required!');
                type3Check = false;
            } else if (!length.match(whl_regex)) {
                $('#type3Error').html('Numbers only! If using decimal then 2 numbers after dot.');
                type3Check = false;
            } else if (!length.match(whlInc_regex)) {
                $('#type3Error').html('Length incorrect!');
                type3Check = false;
            } else {
                $('#type3Error').html('');
                type3Check = true;
            }
        }


        if (skuCheck == true && nameCheck == true && priceCheck == true && switchCheck == true && typeCheck == true && type2Check == true && type3Check == true) {
            $('#form').submit();

        /*  $('#save').click(function() {
                
                $.ajax({
                    type: "POST",
                    url: 'php/main.php',
                    data: {
                        sku: sku,
                        name: name,
                        price: price,
                        type: type,
                        size: size,
                        weight: weight,
                        height: height,
                        width: width,
                        length: length,
                    },
                    success: function(data) {
                        alert(data);
                        alert("Succesful transfer!!!!!!");
                    }
                });
            }); */



        }

    });
});
