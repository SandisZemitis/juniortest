$(document).ready(function() {
    $('#form')[0].reset(); /* Resets form because Firefox saves selected option on page refresh instead of returning to select="selected" option */



    $('#type').on('change', function() {

        var optionText = $('#type option:selected').text();
        if (optionText == 'Furniture') {
            $('#info').html('Please provide all dimensions in CM!');

            $('#addLabel').html('<label for="height" id="labelHeight" class="col-1">Height</label>');

            $('#addType').html('<input type="text" id="height" name="height" class="col-2">');

            $('#addLabel2').html('<label for="width" id="labelWidth" class="col-1">Width</label>');

            $('#addType2').html('<input type="text" id="width" name="width" class="col-2">');

            $('#addLabel3').html('<label for="length" id="labelLength" class="col-1">Length</label>');

            $('#addType3').html('<input type="text" id="length" name="length" class="col-2">');
        } else if (optionText == 'CD') {
            $('#height').remove();
            $('#labelHeight').remove();
            $('#width').remove();
            $('#labelWidth').remove();
            $('#length').remove();
            $('#labelLength').remove();

            $('#info').html('Please provide size in MB!');


            $('#addLabel').html('<label for="size" id="labelSize" class="col-1">Size</label>');

            $('#addType').html('<input type="text" id="size" name="size" class="col-2"><br><br>');
        } else if (optionText == 'Book') {
            $('#height').remove();
            $('#labelHeight').remove();
            $('#width').remove();
            $('#labelWidth').remove();
            $('#length').remove();
            $('#labelLength').remove();

            $('#info').html('Please provide weight in KG!');

            $('#addLabel').html('<label for="weight" id="a" class="col-1">Weight</label>');

            $('#addType').html('<input type="text" id="weight" name="weight" class="col-2"><br><br>');
        }
    });
});
